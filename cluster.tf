resource "ionoscloud_k8s_cluster" "poc_cluster" {
    name = "poc-cluster"
}

resource "ionoscloud_k8s_node_pool" "poc_node_pool" {
  name        = "poc-nodepool"
  k8s_version = ionoscloud_k8s_cluster.poc_cluster.k8s_version
  
  datacenter_id     = ionoscloud_datacenter.poc_dc.id
  k8s_cluster_id    = ionoscloud_k8s_cluster.poc_cluster.id
  cpu_family        = "INTEL_SKYLAKE"
  ram_size          = 2048  
  storage_size      = 20
  availability_zone = "AUTO"
  storage_type      = "SSD"
  node_count        = 1
  cores_count       = 1
}