resource "ionoscloud_datacenter" "poc_dc" {
  name        = "ionos-poc-dc"
  location    = var.location
  description = "We need a datacenter to create a nodepool to use in k8s"
}
