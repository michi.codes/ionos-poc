resource "ionoscloud_user" "michi" {
  count = 0
  first_name              = "Michael"
  last_name               = "Weilbächer"
  email                   = "mail@michi.codes"
  # why the heck do you need to provide a password in cleartext
  password                = "you_whish_I_really_provision_this"
  administrator           = false
  force_sec_auth          = false
  active                  = false
}