terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/35999453/terraform/state/ionos-poc-state"
    lock_address = "https://gitlab.com/api/v4/projects/35999453/terraform/state/ionos-poc-state/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/35999453/terraform/state/ionos-poc-state/lock"
  }
}